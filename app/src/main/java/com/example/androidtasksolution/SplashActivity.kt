package com.example.androidtasksolution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class SplashActivity : AppCompatActivity() {
    private lateinit var timer: CountDownTimer

    companion object {
        // This is the number of milliseconds in a second
        private const val ONE_SECOND = 1000L

        // This is the total time of the splash screen
        private const val COUNTDOWN_TIME = 3000L

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Creates a timer for splash screen
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {

            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                // You can declare your desire activity here to open after finishing splash screen. Like MainActivity
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent);
                finish()
            }
        }

        timer.start()
    }

}