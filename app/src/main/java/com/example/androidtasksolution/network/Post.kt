package com.example.androidtasksolution.network

import android.os.Parcelable
import androidx.lifecycle.LiveData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
        val id: Int,
        val title: String,
        val body:String) : Parcelable {}
