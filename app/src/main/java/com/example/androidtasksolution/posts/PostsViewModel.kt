package com.example.androidtasksolution.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidtasksolution.network.Post
import com.example.androidtasksolution.network.PostsApi
import kotlinx.coroutines.launch

enum class PostsApiStatus { LOADING, ERROR, DONE }
/**
 * The [ViewModel] that is attached to the [PostsFragment].
 */
class PostsViewModel : ViewModel() {

    // The internal MutableLiveData that stores the status of the most recent request
    private val _status = MutableLiveData<PostsApiStatus>()

    // The external immutable LiveData for the request status
    val status: LiveData<PostsApiStatus>
        get() = _status

    // Internally, we use a MutableLiveData, because we will be updating the List of posts
    // with new values
    private val _posts = MutableLiveData<List<Post>>()

    // The external LiveData interface to the property is immutable, so only this class can modify
    val posts: LiveData<List<Post>>
        get() = _posts

    /**
     * Call getPosts() on init so we can display status immediately.
     */
    init {
        getPosts()
    }

    /**
     * Gets posts from the Posts API Retrofit service and
     * updates the [Post] [List] and [PostsApiStatus] [LiveData]. The Retrofit service
     * returns a coroutine Deferred, which we await to get the result of the transaction.
     */
     private fun getPosts() {
        viewModelScope.launch {
            _status.value = PostsApiStatus.LOADING
            try {
                _posts.value = PostsApi.retrofitService.getPosts()
                _status.value = PostsApiStatus.DONE
            } catch (e: Exception) {
                _status.value = PostsApiStatus.ERROR
                _posts.value = ArrayList()
            }
        }
    }

}
