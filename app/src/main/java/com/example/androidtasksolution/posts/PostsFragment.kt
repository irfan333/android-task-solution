package com.example.androidtasksolution.posts

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidtasksolution.databinding.FragmentPostsBinding

class PostsFragment : Fragment() {

    /**
     * Lazily initialize our [PostsViewModel].
     */
    private val viewModel: PostsViewModel by lazy {
        ViewModelProvider(this).get(PostsViewModel::class.java)
    }


    /**
     * Inflates the layout with Data Binding, sets its lifecycle owner to the PostsFragment
     * to enable Data Binding to observe LiveData, and sets up the RecyclerView with an adapter.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentPostsBinding.inflate(inflater)

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the PostsViewModel
        binding.viewModel = viewModel

        // Sets the adapter of the RecyclerView
        binding.rvPosts.adapter = PostsAdapter()
        binding.rvPosts.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        // Observe the posts LiveData
        /*viewModel.posts.observe(viewLifecycleOwner, Observer {
            if ( null != it ) {

            }
        })*/

        return binding.root
    }

}